/*
  Analog input, analog output, serial output

  Reads an analog input pin, maps the result to a range from 0 to 255 and uses
  the result to set the pulse width modulation (PWM) of an output pin.
  Also prints the results to the Serial Monitor.

  The circuit:
  - potentiometer connected to analog pin 0.
    Center pin of the potentiometer goes to the analog pin.
    side pins of the potentiometer go to +5V and ground
  - LED connected from digital pin 9 to ground

  created 29 Dec. 2008
  modified 9 Apr 2012
  by Tom Igoe

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/AnalogInOutSerial
*/

byte FrameSync[] = {0xFE, 0x6B, 0x28, 0x40};

byte V0;
byte V1;
byte V2;
byte V3;
byte V4;
byte V5;

void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);
}

void loop() {
  // read the analog in value:
  V0 = analogRead(A0);
  V1 = analogRead(A1);
  V2 = analogRead(A2);
  V3 = analogRead(A3);
  V4 = analogRead(A4);
  V5 = analogRead(A5);

  // print the results to the Serial Monitor:
  Serial.write(FrameSync, 4);
  Serial.write(V0);
  Serial.write(V1);
  Serial.write(V2);
  Serial.write(V3);
  Serial.write(V4);
  Serial.write(V5);

  // wait 2 milliseconds before the next loop for the analog-to-digital
  // converter to settle after the last reading:
  delay(2);
}
