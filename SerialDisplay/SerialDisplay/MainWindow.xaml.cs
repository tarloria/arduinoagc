﻿using System;
using System.Threading.Tasks;
using System.Windows;

namespace SerialDisplay
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var PortNames = System.IO.Ports.SerialPort.GetPortNames();
            for (int i = 0; i < PortNames.Length; i++)
            {
                ListViewPortNames.Items.Add(PortNames[i]);
            }
            ListViewPortNames.SelectedIndex = 0;
        }

        System.IO.Ports.SerialPort Serial1;

        TaskFactory f = new TaskFactory();

        byte[] Frame = new byte[10];

        private void ButtonConnect_Click(object sender, RoutedEventArgs e)
        {
            StackPanelConnect.Visibility = Visibility.Collapsed;
            StackPanelScale.Visibility = Visibility.Visible;

            string PortName = (string)ListViewPortNames.SelectedItem;
            Serial1 = new System.IO.Ports.SerialPort(PortName, 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
            Serial1.Open();
            f.StartNew(() =>
            {
                while (true)
                {
                    for (int i = 0; i < (Frame.Length - 1); i++)
                    {
                        Frame[i] = Frame[i + 1];
                    }
                    byte ByteTemp = (byte)Serial1.ReadByte();
                    Frame[Frame.Length - 1] = ByteTemp;
                    if ((Frame[0] == 0xFE) &&
                        (Frame[1] == 0x6B) &&
                        (Frame[2] == 0x28) &&
                        (Frame[3] == 0x40))
                    {
                        if (Application.Current != null)
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                double Scale = 0;
                                string ScaleStr = TextBoxScale.Text;
                                if (!String.IsNullOrEmpty(ScaleStr))
                                {
                                    Double.TryParse(ScaleStr, out Scale);
                                }

                                TextBlockA0V.Text = "A0:" + (Frame[4] * Math.Pow(2, -8) * 5).ToString("F3") + " V";
                                TextBlockA1V.Text = "A1:" + (Frame[5] * Math.Pow(2, -8) * 5).ToString("F3") + " V";
                                TextBlockA2V.Text = "A2:" + (Frame[6] * Math.Pow(2, -8) * 5).ToString("F3") + " V";
                                TextBlockA3V.Text = "A3:" + (Frame[7] * Math.Pow(2, -8) * 5).ToString("F3") + " V";
                                TextBlockA4V.Text = "A4:" + (Frame[8] * Math.Pow(2, -8) * 5).ToString("F3") + " V";
                                TextBlockA5V.Text = "A5:" + (Frame[9] * Math.Pow(2, -8) * 5).ToString("F3") + " V";

                                TextBlockA0dB.Text = "A0:" + (Frame[4] * Math.Pow(2, -8) * Scale).ToString("F3");
                                TextBlockA1dB.Text = "A1:" + (Frame[5] * Math.Pow(2, -8) * Scale).ToString("F3");
                                TextBlockA2dB.Text = "A2:" + (Frame[6] * Math.Pow(2, -8) * Scale).ToString("F3");
                                TextBlockA3dB.Text = "A3:" + (Frame[7] * Math.Pow(2, -8) * Scale).ToString("F3");
                                TextBlockA4dB.Text = "A4:" + (Frame[8] * Math.Pow(2, -8) * Scale).ToString("F3");
                                TextBlockA5dB.Text = "A5:" + (Frame[9] * Math.Pow(2, -8) * Scale).ToString("F3");

                                ProgressBar0.Value = Frame[4] * Math.Pow(2, -8) * 100;
                                ProgressBar1.Value = Frame[5] * Math.Pow(2, -8) * 100;
                                ProgressBar2.Value = Frame[6] * Math.Pow(2, -8) * 100;
                                ProgressBar3.Value = Frame[7] * Math.Pow(2, -8) * 100;
                                ProgressBar4.Value = Frame[8] * Math.Pow(2, -8) * 100;
                                ProgressBar5.Value = Frame[9] * Math.Pow(2, -8) * 100;
                            });
                        }
                    }
                }
            });
        }
    }
}
